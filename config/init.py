import os
import re
import json
import pandas
import math
import pickle
import zipfile
import distutils.spawn
import numpy as np
import scipy.stats as stats
import seaborn as sns

from IPython.display import display

import matplotlib
import matplotlib.pyplot as plt

from IPython.display import HTML
from IPython.display import display, Markdown, Latex

from jupyterngsplugin.utils.errors import check_errors_from_logs

###############################################################
#
#    Update cutoff values
#
###############################################################

# log2(FoldChange)
fc = 2.0

# max FDR (adjusted P-Value)
fdr = 0.05

###############################################################
#
#    Project absolute path
#
###############################################################

WORKDIR = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

###############################################################
#
#    Update genome files and indexes path
#
# If indexes and reference bed files does not exist can be created using 
# the notebooks but you need to have writing permission in the GENOME dir
#
###############################################################
GENOME = '/gfs/data/genomes/igenomes/Mus_musculus/UCSC/mm9'
GENOME_NAME = 'mm9'
BWA_INDEX = os.path.join(GENOME, 'BWA')
GENOME_FASTA = os.path.join(GENOME, 'genome.fa')
GENOME_GTF = os.path.join(GENOME, 'genes.gtf')
GENOME_BED = os.path.join(GENOME, 'genes.bed')
GENOME_MAPPABLE_SIZE = 'mm'
GENOME_BLACKLIST = os.path.join(GENOME, 'mm9-blacklist.bed')

###############################################################
#
#    Dataset (experiment) to analyze
#
# The path is $WORKDIR/data/$DATASET
#
# To use multiple datasets (experiments) this variable should be overwritten
# in the notebooks
#
###############################################################

DATASET = 'PRJNA481982'

###############################################################
#
#    Docker configuration
#
###############################################################

DOCKER = True

###############################################################
#
#    cwl-runner with absolute path if necesary 
#
###############################################################

CWLRUNNER_TOOL = 'cwl-runner'
CWLRUNNER_TOOL_PATH = distutils.spawn.find_executable(CWLRUNNER_TOOL)
if not CWLRUNNER_TOOL_PATH:
    print('WARNING: %s not in path' % (CWLRUNNER_TOOL))
    print('Install:')
    print('pip install cwltool')
    print('pip install cwl-runner')
else:
    CWLRUNNER = CWLRUNNER_TOOL_PATH
if not DOCKER:
    CWLRUNNER = CWLRUNNER + ' --no-container '

###############################################################

CONFIG = os.path.join(WORKDIR,'config')
if not os.path.exists(CONFIG):
    os.mkdir(CONFIG)

DATA = os.path.join(WORKDIR,'data')
if not os.path.exists(DATA):
    os.mkdir(DATA)
    
BIN = os.path.join(WORKDIR,'bin')
if not os.path.exists(BIN):
    os.mkdir(BIN)
    
RESULTS = os.path.join(WORKDIR,'results')
if not os.path.exists(RESULTS):
    os.mkdir(RESULTS)
    
NOTEBOOKS = os.path.join(WORKDIR,'notebooks')
if not os.path.exists(NOTEBOOKS):
    os.mkdir(NOTEBOOKS)
    
SRC = os.path.join(WORKDIR,'src')
if not os.path.exists(SRC):
    os.mkdir(SRC)
    
TMP = os.path.join(WORKDIR,'tmp')
if not os.path.exists(TMP):
    os.mkdir(TMP)
    
CWLURL = '/gfs/veraalva/Work/Developer/Python/cwl-workflow'    
CWLTOOLS = os.path.join(CWLURL, 'tools')
CWLWORKFLOWS = os.path.join(CWLURL, 'workflows')

CWLRUNNER = CWLRUNNER + ' --tmp-outdir-prefix=' + TMP + '/ --tmpdir-prefix=' + TMP + '/ '

